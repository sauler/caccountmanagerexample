/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of CAccountManagerExample for AQQ                         *
 *                                                                             *
 * CAccountManagerExample plugin is free software: you can redistribute it     *
 * and/or  modify it under the terms of the GNU General Public License as      *
 * published by the Free Software Foundation; either version 3,                *
 * or (at your option) any later version.                                      *
 *                                                                             *
 * CAccountManagerExample is distributed in the hope that it will be useful,   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

 #pragma once
#include <vcl.h>
#include "SDK\Account.h"

class CMyAccount: public CAccount
{
public:
	CMyAccount(UnicodeString Name);
    ~CMyAccount();

    virtual void OnDefaultEvent();
    virtual void OnNewEvent();
    virtual void OnEditEvent();
    virtual void OnDeleteEvent();
    virtual void OnChangePassEvent();
};
