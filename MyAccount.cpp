/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of CAccountManagerExample for AQQ                         *
 *                                                                             *
 * CAccountManagerExample plugin is free software: you can redistribute it     *
 * and/or  modify it under the terms of the GNU General Public License as      *
 * published by the Free Software Foundation; either version 3,                *
 * or (at your option) any later version.                                      *
 *                                                                             *
 * CAccountManagerExample is distributed in the hope that it will be useful,   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

 #include "MyAccount.h"
#include "AQQ.h"

CMyAccount::CMyAccount(UnicodeString Name)
{
	this->Name = Name;
	this->IconID = -1;
    this->CanCreate = true;
    this->CanEdit = true;
    this->CanDelete = true;
    this->CanChangePass = true;
    this->DefaultEvent = true;
    this->Create();
}

CMyAccount::~CMyAccount()
{
    this->Delete();
}

void CMyAccount::OnDefaultEvent()
{
    AQQ::Functions::ShowMessage(0, "Default");
}

void CMyAccount::OnNewEvent()
{
	AQQ::Functions::ShowMessage(0, "New " + this->Name + " account");
}

void CMyAccount::OnEditEvent()
{
	AQQ::Functions::ShowMessage(0, "Edit " + this->Name + " account");
}

void CMyAccount::OnDeleteEvent()
{
	AQQ::Functions::ShowMessage(0, "Delete " + this->Name + " account");
}

void CMyAccount::OnChangePassEvent()
{
    AQQ::Functions::ShowMessage(0, "ChangePass for "  + this->Name + " account");
}
